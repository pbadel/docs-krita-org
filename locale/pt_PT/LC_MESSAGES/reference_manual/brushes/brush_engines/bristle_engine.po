# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 14:47+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: optionsize icons depleção demão image\n"
"X-POFile-SpellExtra: optionrotation Depleção bristlebrush images ref\n"
"X-POFile-SpellExtra: guilabel deplexão\n"

#: ../../<generated>:1
msgid "Weighted saturation"
msgstr "Saturação ponderada"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:0
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.3-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.3-1.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:0
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.3-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.3-2.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:1
msgid "The Bristle Brush Engine manual page."
msgstr "A página de manual do Motor de Pincéis de Escova."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:18
msgid "Bristle Brush Engine"
msgstr "Motor de Pincéis em Escova"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Brush Engine"
msgstr "Motor de Pincéis"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Hairy Brush Engine"
msgstr "Motor de Pincéis com Pêlos"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Sumi-e"
msgstr "Sumi-e"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:21
msgid ".. image:: images/icons/bristlebrush.svg"
msgstr ".. image:: images/icons/bristlebrush.svg"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:22
msgid ""
"A brush intended to mimic real-life brushes by drawing the trails of their "
"lines or bristles."
msgstr ""
"Um pincel que pretende imitar as escovas na vida real, desenhando os rastos "
"das suas linhas ou pêlos."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:25
msgid "Brush Tip"
msgstr "Tamanho do Pincel"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:27
msgid "Simply put:"
msgstr "De forma simples:"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:29
msgid "The brush tip defines the areas with bristles in them."
msgstr "A ponta do pincel define as áreas com pêlos de escova neles."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:30
msgid ""
"Lower opacity areas have lower-opacity bristles. With this brush, this may "
"give the illusion that lower-opacity areas have fewer bristles."
msgstr ""
"As áreas com menores opacidades têm pêlos de menor opacidade. Com este "
"pincel, isso dará a ilusão que as áreas com menor opacidade têm menos pêlos."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:31
msgid ""
"The :ref:`option_size` and :ref:`option_rotation` dynamics affect the brush "
"tip, not the bristles."
msgstr ""
"Os parâmetros dinâmicos :ref:`option_size` e :ref:`option_rotation` afectam "
"a ponta do pincel, não os pêlos."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:33
msgid "You can:"
msgstr "Poderá:"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:35
msgid ""
"Use different shapes for different effects. Be aware that complex brush "
"shapes will draw more slowly though, while the effects aren't always visible "
"(since in the end, you're passing over an area with a certain number of "
"bristles)."
msgstr ""
"Usa diferentes formas para diferentes efeitos. Tenha atenção que as formas "
"complexas serão mais lentas para desenhar; embora os efeitos não estejam "
"sempre visíveis (dado que, no fim, irá passar sobre uma área com um dado "
"número de pêlos da escova)."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:36
msgid ""
"To decrease bristle density, you can also just use an autobrush and decrease "
"the brush tip's density, or increase its randomness."
msgstr ""
"Para diminuir a densidade das escovas, poderá também usar um pincel "
"automático e diminuir a densidade da ponta do pincel, ou então aumentar a "
"sua aleatoriedade."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:39
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.1.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.1.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:41
msgid "Bristle Options"
msgstr "Opções da Escova"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:43
msgid "The core of this particular brush-engine."
msgstr "As bases deste motor de pincéis em particular."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:46
msgid ""
"Think of it as pressing down on a brush to make the bristles further apart."
msgstr ""
"Pense nisto como carregar com força num pincel para que os pêlos se afastem "
"mais."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:48
msgid ""
"Larger values basically give you larger brushes and larger bristle spacing. "
"For example, a value of 4 will multiply your base brush size by 4, but the "
"bristles will be 4 times more spaced apart."
msgstr ""
"Os valores mais elevados irão criar-lhe pincéis maiores e um espaço maior "
"entre pêlos do pincel. Por exemplo, um valor igual a 4 irá multiplicar o "
"tamanho do seu pincel de base por 4, mas os pêlos ficarão 4 vezes mais "
"espaçados."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:49
msgid ""
"Use smaller values if you want a \"dense\" brush, i.e. you don't want to see "
"so many bristles within the center."
msgstr ""
"Use valores menores se quiser um pincel \"denso\", i.e. se não quiser ver "
"tantas escovas perto do centro."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:50
msgid "Scale"
msgstr "Escala"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:50
msgid ""
"Negative values have the same effect as corresponding positive values: -1.00 "
"will look like 1.00, etc."
msgstr ""
"Os valores negativos têm o mesmo efeito que os respectivos valores "
"positivos; -1,00 terá o mesmo aspecto que 1,00, etc."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:53
msgid "Adds a jaggy look to the trailing lines."
msgstr "Adiciona um visual tremido às linhas finais."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:55
msgid "At 0.00, all the bristles basically remain completely parallel."
msgstr ""
"A 0,00, todos os pêlos das escovas ficam basicamente paralelos por completo."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:56
msgid "Random Offset"
msgstr "Deslocamento Aleatório"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:56
msgid ""
"At other values, the bristles are offset randomly. Large values will "
"increase the brush size a bit because of the bristles spreading around, but "
"not by much."
msgstr ""
"Com outros valores, os pêlos são desviados de forma aleatória. Os valores "
"mais elevados irão aumentar um bocado o tamanho do pincel, devido ao facto "
"de os pêlos se espalharem um pouco, mas não muito."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:57
msgid "Negative values have the same effect as corresponding positive values."
msgstr ""
"Os valores negativos têm o mesmo efeito que os respectivos valores positivos."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:58
msgid "Shear"
msgstr "Inclinar"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:59
msgid ""
"Shear introduces an angle to your brush, as though you're drawing with an "
"oval brush (or the side of a round brush)."
msgstr ""
"A inclinação introduz um ângulo ao seu pincel, como se estivesse a desenhar "
"com um pincel oval (ou com a parte lateral de um pincel redondo)."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:61
msgid "Density"
msgstr "Densidade"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:61
msgid ""
"This controls the density of bristles. Scale takes a number of bristles and "
"expands or compresses them into a denser area, whereas density takes a fixed "
"area and determines the number of bristles in it. See the difference?"
msgstr ""
"Isto controla a densidade dos pêlos. A escala recebe um número de pêlos e "
"expande-os ou comprime-os numa área mais densa, enquanto a densidade tem em "
"conta uma área fixa e define o número de pêlos nela. Percebe a diferença?"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:64
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.2-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.2-1.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:66
msgid ""
"This one maps \"Scale\" to mouse speed, thus simulating pressure with a "
"graphics tablet!"
msgstr ""
"Isto associa a \"Escala\" à velocidade do rato, simulando assim a pressão "
"com uma tablete de desenho!"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:68
msgid "Mouse Pressure"
msgstr "Pressão do Rato"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:68
msgid ""
"Rather, it uses the \"distance between two events\" to determine scale. "
"Faster drawing, larger distances."
msgstr ""
"Em vez disso, usa a \"distância entre dois eventos\" para determinar a "
"escala. Quanto mais rápido o desenho, maiores as distâncias."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:69
msgid ""
"This doesn't influence the \"pressure\" input for anything else (size, "
"opacity, rotation etc.) so you still have to map those independently to "
"something else."
msgstr ""
"Isto não influencia a variável de \"pressão\" para nada mais (tamanho, "
"opacidade, rotação, etc.) pelo que terá de associar esses de forma "
"independente para outra coisa qualquer."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:70
msgid "Threshold"
msgstr "Limiar"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:71
msgid ""
"This is a tablet feature. When you turn this on, only bristles that are able "
"to \"touch the canvas\" will be painted."
msgstr ""
"Esta é uma funcionalidade da tablete. Quando activar isto, só os pêlos\" que "
"conseguem \"tocar na área de desenho\" é que serão pintados."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:72
msgid "Connect Hairs"
msgstr "Ligar os Fios"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:73
msgid "The bristles get connected. See for yourself."
msgstr "Os pêlos da escova ficam ligados. Veja você mesmo."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:74
msgid "Anti-Aliasing"
msgstr "'Anti-Aliasing'"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:75
msgid "This will decrease the jaggy-ness of the lines."
msgstr "Isto irá diminuir as tremuras das linhas."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:77
msgid "Composite Bristles"
msgstr "Escovas Compostas"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:77
msgid ""
"This \"composes the bristle colors within one dab,\" but explains that the "
"effect is \"probably subtle\"."
msgstr ""
"Isto \"compõe as cores dos pêlos das escovas dentro de uma demão\", mas "
"convém explicar que o efeito será \"provavelmente subtil\"."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:80
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.2-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.2-2.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:82
msgid "Ink Depletion"
msgstr "Depleção da Tinta"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:84
msgid ""
"This simulated ink depletion over drawing time. The value dictates how long "
"it will take. The curve dictates the speed."
msgstr ""
"Isto simula a depleção da tinta ao longo do tempo de desenho. O valor dita "
"durante quanto tempo decorrerá o efeito. A curva dita a velocidade."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:86
msgid "Opacity"
msgstr "Opacidade"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:87
msgid "The brush will go transparent to simulate ink-depletion."
msgstr "O pincel ficará transparente para simular a depleção da tinta."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:89
msgid "The brush will be desaturated to simulate ink-depletion."
msgstr "O pincel perderá a saturação para simular a depleção da tinta."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:91
msgid "Saturation"
msgstr "Saturação"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:94
msgid ""
"The brush will pick up colors from other brushes. You don't need to have :"
"guilabel:`Ink depletion` checked to activate this option, you just have to "
"check :guilabel:`Soak ink`. What this does is cause the bristles of the "
"brush to take on the colors of the first area they touch. Since the Bristle "
"brush is made up of independent bristles, you can basically take on several "
"colors at the same time."
msgstr ""
"O pincel irá apanhar as cores dos outros pincéis. Não será necessário ter "
"assinalada a opção de :guilabel:`Depleção da tinta` para activar esta opção, "
"simplesmente tem de assinalar a :guilabel:`Tinta ensopada`. O que isso "
"origina é fazer com que os pêlos dos pincéis apanhem as cores da primeira "
"área em que tocam. Dado que o pincel de Escova é composto por pêlos "
"independentes, poderá basicamente apanhar diversas cores ao mesmo tempo."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:98
msgid ""
"It will only take colors in the unscaled area of the brush, so if you're "
"using a brush with 4.00 scale for example, it will only take the colors in "
"the 1/4 area closest to the center."
msgstr ""
"Só irá usar as cores na área sem escala do pincel; por isso, se estiver por "
"exemplo a usar um pincel com uma escala de 4,00, só irá ter em conta as "
"cores em 1/4 da área mais próximo do centro."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:99
msgid "When the source is transparent, the bristles take black color."
msgstr "Quando a origem é transparente, os pêlos usam a cor preta."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:104
msgid "Soak Ink"
msgstr "Tinta Ensopada"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:104
msgid ""
"Be aware that this feature is a bit buggy though. It's supposed to take the "
"color from the current layer, but some buggy behavior causes it to often use "
"the last layer you've painted on (with a non-Bristle brush?) as source. To "
"avoid these weird behaviors, stick to just one layer, or paint something on "
"the current active layer first with another brush (such as a Pixel brush)."
msgstr ""
"Tenha atenção que esta funcionalidade tem alguns problemas. É suposto ter em "
"conta a cor da camada actual, mas algum comportamento problemático faz com "
"que ele use a última camada sobre a qual pintou (com um pincel não-Escova?) "
"como origem. Para evitar estes comportamentos estranhos, fique apenas com "
"uma camada, ou pinte algo sobre a camada activa de momento primeiro com "
"outro pincel (como um pincel de Pixels)."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:107
msgid "Works by modifying the saturation with the following:"
msgstr "Funciona através da modificação da saturação com o seguinte:"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:109
msgid "Pressure weight"
msgstr "Peso da pressão"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:110
msgid "Bristle length weight"
msgstr "Peso do comprimento dos pêlos"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:111
msgid "Bristle ink amount weight"
msgstr "Peso da quantidade de tinta do espinho"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:112
msgid "Ink depletion curve weight"
msgstr "Peso da curva de deplexão da tinta"
