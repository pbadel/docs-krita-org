msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_settings.pot\n"

#: ../../reference_manual/brushes/brush_settings.rst:5
msgid "Brush Settings"
msgstr "笔刷选项"

#: ../../reference_manual/brushes/brush_settings.rst:7
msgid "Overall Brush Settings for the various brush engines."
msgstr "简要介绍各种笔刷引擎的选项。"

#: ../../reference_manual/brushes/brush_settings.rst:9
msgid "Contents:"
msgstr "目录："
