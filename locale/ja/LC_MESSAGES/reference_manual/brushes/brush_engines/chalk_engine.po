msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:1
msgid "The Chalk Brush Engine manual page."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:17
msgid "Chalk Brush Engine"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:21
msgid ""
"This brush engine has been removed in 4.0. There are other brush engines "
"such as pixel that can do everything this can...plus more."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:23
msgid ""
"Apparently, the Bristle brush engine is derived from this brush engine. Now, "
"all of :program:`Krita's` brushes have a great variety of uses, so you must "
"have tried out the Chalk brush and wondered what it is for. Is it nothing "
"but a pixel brush with opacity and saturation fade options? As per the "
"developers this brush uses a different algorithm than the Pixel Brush, and "
"they left it in here as a simple demonstration of the capabilities of :"
"program:`Krita's` brush engines."
msgstr ""

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:26
msgid ""
"So there you go, this brush is here for algorithmic demonstration purposes. "
"Don't lose sleep because you can't figure out what it's for, it Really "
"doesn't do much. For the sake of description, here's what it does:"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:29
msgid ".. image:: images/brushes/Krita-tutorial7-C.png"
msgstr ""

#: ../../reference_manual/brushes/brush_engines/chalk_engine.rst:30
msgid ""
"Yeah, that's it, a round brush with some chalky texture, and the option to "
"fade in opacity and saturation. That's it."
msgstr ""
