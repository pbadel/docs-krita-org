# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-05-07 10:17+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: muislinks"

#: ../../<rst_epilog>:30
msgid ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"
msgstr ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"

#: ../../reference_manual/tools/polygon.rst:1
msgid "Krita's polygon tool reference."
msgstr "Verwijzing naar hulpmiddel Polygoon van Krita."

#: ../../reference_manual/tools/polygon.rst:10
msgid "Tools"
msgstr "Hulpmiddelen"

#: ../../reference_manual/tools/polygon.rst:10
msgid "Polygon"
msgstr "Polygoon"

#: ../../reference_manual/tools/polygon.rst:15
msgid "Polygon Tool"
msgstr "Hulpmiddel Polygoon"

#: ../../reference_manual/tools/polygon.rst:17
msgid "|toolpolygon|"
msgstr ""

#: ../../reference_manual/tools/polygon.rst:19
msgid ""
"With this tool you can draw polygons. Click the |mouseleft| to indicate the "
"starting point and successive vertices, then double-click or press the :kbd:"
"`Enter` key to connect the last vertex to the starting point."
msgstr ""

#: ../../reference_manual/tools/polygon.rst:21
msgid ":kbd:`Shift + Z` undoes the last clicked point."
msgstr ""

#: ../../reference_manual/tools/polygon.rst:24
msgid "Tool Options"
msgstr "Hulpmiddelopties"
