# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
# Lo Fernchu <lofernchu@zonnet.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:21+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: ../../<generated>:1
msgid "Soft-proofing"
msgstr "Soft-proofing"

#: ../../general_concepts/colors/profiling_and_callibration.rst:1
msgid "Color Models in Krita"
msgstr "Kleurmodellen in Krita"

#: ../../general_concepts/colors/profiling_and_callibration.rst:13
msgid "Color"
msgstr "Kleur"

#: ../../general_concepts/colors/profiling_and_callibration.rst:13
msgid "Profiling"
msgstr "Profileren"

#: ../../general_concepts/colors/profiling_and_callibration.rst:13
msgid "Calibration"
msgstr "Kalibratie"

#: ../../general_concepts/colors/profiling_and_callibration.rst:18
msgid "Profiling and Calibration"
msgstr "Profileren en kalibratie"

#: ../../general_concepts/colors/profiling_and_callibration.rst:20
msgid ""
"So to make it simple, a color profile is just a file defining a set of "
"colors inside a pure XYZ color cube. This \"color set\" can be used to "
"define different things:"
msgstr ""
"Om het eenvoudig te houden, een kleuren-profiel is gewoon maar een bestand "
"dat een verzameling kleuren definieert in een pure XYZ kleurenkubus. Deze "
"\"verzameling kleuren\" kan gebruikt worden om verschillende dingen te "
"definiëren:"

#: ../../general_concepts/colors/profiling_and_callibration.rst:23
msgid "the colors inside an image"
msgstr "de kleuren in een afbeelding"

#: ../../general_concepts/colors/profiling_and_callibration.rst:25
msgid "the colors a device can output"
msgstr "de kleuren die een apparaat kan produceren"

#: ../../general_concepts/colors/profiling_and_callibration.rst:27
msgid ""
"Choosing the right workspace profile to use depends on how much colors you "
"need and on the bit depth you plan to use. Imagine a line with the whole "
"color spectrum from pure black (0,0,0) to pure blue (0,0,1) in a pure XYZ "
"color cube. If you divide it choosing steps at a regular interval, you get "
"what is called a linear profile, with a gamma=1 curve represented as a "
"straight line from 0 to 1. With 8bit/channel bit depth, we have only 256 "
"values to store this whole line. If we use a linear profile as described "
"above to define those color values, we will miss some important visible "
"color change steps and have a big number of values looking the same (leading "
"to posterization effect)."
msgstr ""
"De keuze van de juiste werkruimte profiel is afhankelijk van hoeveel kleuren "
"die u nodig heeft en de bitdiepte die u wilt gebruiken. Stelt u zich een "
"lijn voor met het hele kleurenspectrum die van puur zwart (0,0,0) tot puur "
"blauw (0,0,1) in een pure XYZ kleurenkubus gaat. Als u het met regelmatige "
"stapjes verdeelt, dan krijgt u wat men noemt een lineair profiel, met een "
"gamma=1 curve voorgesteld als een rechte lijn van 0 naar 1. Met een 8 bit/"
"kanaal bit diepte, kunnen we alleen 256 waarden gebruiken om deze hele lijn "
"op te slaan. Als we een lineair profiel zoals hierboven beschreven gebruiken "
"om die kleurwaarden te definiëren, dan zullen we enkele belangrijke "
"zichtbare veranderingen in kleurverlopen missen terwijl een groot aantal "
"kleuren hetzelfde uitzien (wat een reductie in schakeringen geeft)."

#: ../../general_concepts/colors/profiling_and_callibration.rst:33
msgid ""
"This is why was created the sRGB profile to fit more different colors in "
"this limited amount of values, in a perceptually regular grading, by "
"applying a custom gamma curve (see picture here: https://en.wikipedia.org/"
"wiki/SRGB) to emulate the standard response curve of old CRT screens. So "
"sRGB profile is optimized to fit all colors that most common screen can "
"reproduce in those 256 values per R/G/B channels. Some other profiles like "
"Adobe RGB are optimized to fit more printable colors in this limited range, "
"primarily extending cyan-green hues. Working with such profile can be useful "
"to improve print results, but is dangerous if not used with a properly "
"profiled and/or calibrated good display. Most common CMYK workspace profile "
"can usually fit all their colors within 8bit/channel depth, but they are all "
"so different and specific that it's usually better to work with a regular "
"RGB workspace first and then convert the output to the appropriate CMYK "
"profile."
msgstr ""
"Dit is de reden waarom men het sRGB profiel creëerde zodat er meer "
"verschillende kleuren in dit beperkt aantal waarden patste, in een  "
"perceptueel regelmatig kleurverloop, door een aangepaste gamma curve (zie de "
"afbeelding hier: https://en.wikipedia.org/wiki/SRGB) te gebruiken om de "
"standaard response curve van oude CRT schermen na te bootsen. Dus het sRGB "
"profiel is zodanig geoptimaliseerd dat daarin alle kleuren passen dat de "
"meeste gewone schermen kunnen reproduceren in die 256 waarden per R/G/B "
"kanalen. Sommige andere profielen zoals Adobe RGB zijn zodanig "
"geoptimaliseerd dat er meer afdrukbare kleuren in die beperkte ruimte "
"passen, voornamelijk door cyaan-groen tinten uit te breiden. Om afdruk-"
"resultaten te verbeteren kan het handig zijn om met een dergelijk profiel te "
"werken, maar het is wel gevaarlijk als men het zonder een goed profiel "
"gebruikt en/of zonder een gekalibreerd goed scherm. Bij de meest voorkomende "
"CMYK werkruimte profielen past meestal al hun kleuren in 8 bit/kanaal "
"diepte, maar ze zijn allemaal zo verschillend en specifiek dat het meestal "
"beter is om eerst met een RGB werkruimte te werken om het daarna het "
"converteren naar het juiste CMYK profiel."

#: ../../general_concepts/colors/profiling_and_callibration.rst:38
msgid ""
"Starting with 16bit/channel, we already have 65536 values instead of 256, so "
"we can use workspace profiles with higher gamut range like Wide-gamut RGB or "
"Pro-photo RGB, or even unlimited gamut like scRGB."
msgstr ""
"Bij 16 bit/kanaal hebben we al 65536 waarden in plaats van 256, zodat we "
"werkruimte-profielen kunnen gebruiken die een grotere kleuren-bereik hebben "
"zoals Wide-gamut RGB of Pro-photo RGB, of zelfs ongelimiteerde bereiken "
"zoals scRGB."

#: ../../general_concepts/colors/profiling_and_callibration.rst:40
msgid ""
"But sRGB being a generic profile (even more as it comes from old CRT "
"specifications...), there are big chances that your monitor have actually a "
"different color response curve, and so color profile. So when you are using "
"sRGB workspace and have a proper screen profile loaded (see next point), "
"Krita knows that the colors the file contains are within the sRGB color "
"space, and converts those sRGB values to corresponding color values from "
"your monitor profile to display the canvas."
msgstr ""
"Maar omdat sRGB een algemeen profiel is (nog erger omdat het uit de oude CRT "
"specificaties komt...), is er een grote kans dat uw beeldscherm een andere "
"kleur response curve heeft, en dus ook een ander kleurenprofiel. Als u dus "
"een sRGB werkruimte gebruikt en een goed scherm-profiel heeft geladen (zie "
"volgende punt), dan weetKrita dat de kleuren in het bestand in de sRGB "
"kleurruimte zijn, en converteert die sRGB waarden naar corresponderende "
"kleuren van uw beeldscherm profiel om het canvas te tonen."

#: ../../general_concepts/colors/profiling_and_callibration.rst:43
msgid ""
"Note that when you export your file and view it in another software, this "
"software has to do two things:"
msgstr ""
"Merk op dat wanneer u uw bestand exporteert en het bekijkt in andere "
"software, deze software twee dingen moet doen:"

#: ../../general_concepts/colors/profiling_and_callibration.rst:45
msgid ""
"read the embed profile to know the \"good\" color values from the file "
"(which most software do nowadays; when they don't they usually default to "
"sRGB, so in the case described here we're safe )"
msgstr ""
"Het ingebedde profiel inlezen om de \"juiste\" kleurwaarden in het bestand "
"te weten te komen (wat tegenwoordig meestal software is; wat meestal niet "
"standaard in sRGB is, zodat we in dit beschreven geval veilig zijn )"

#: ../../general_concepts/colors/profiling_and_callibration.rst:46
msgid ""
"and then convert it to the profile associated to the monitor (which very few "
"software actually does, and just output to sRGB.. so this can explain some "
"viewing differences most of the time)."
msgstr ""
"Om het daarna te converteren naar het bij het beeldscherm horende profiel "
"(wat maar weinig programma's werkelijk doen, en gewoon converteren naar "
"sRGB.. dit kan dus de kleine weergave verschillen verklaren die vaak "
"optreden)."

#: ../../general_concepts/colors/profiling_and_callibration.rst:48
msgid "Krita uses profiles extensively, and comes bundled with many."
msgstr ""
"Krita maakt veel gebruik van profielen, en er worden veel daarvan "
"meegeleverd."

#: ../../general_concepts/colors/profiling_and_callibration.rst:50
msgid ""
"The most important one is the one of your own screen. It doesn't come "
"bundled, and you have to make it with a color profiling device. In case you "
"don't have access to such a device, you can't make use of Krita's color "
"management as intended. However, Krita does allow the luxury of picking any "
"of the other bundled profiles as working spaces."
msgstr ""
"De meest belangrijke is die van uw eigen beeldscherm. En die wordt niet "
"meegeleverd, die moet u zelf maken met een kleurprofiel-apparaat. In het "
"geval dat u zo een apparaat niet ter beschikking heeft, dan kunt het "
"kleurbeheer van Krita niet gebruiken zoals het bedoeld is. Maar, gelukkig "
"staat Krita het toe om een van de meegeleverde profielen als werkruimte te "
"gebruiken."

#: ../../general_concepts/colors/profiling_and_callibration.rst:54
msgid "Profiling devices"
msgstr "Profilering-apparaten"

#: ../../general_concepts/colors/profiling_and_callibration.rst:56
msgid ""
"Profiling devices, called Colorimeters, are tiny little cameras of a kind "
"that you connect to your computer via an usb, and then you run a profiling "
"software (often delivered alongside of the device)."
msgstr ""
"Profilering-apparaten, ook wel Colorimeters genoemd, zijn een soort hele "
"kleine cameras die u via usb verbindt met uw computer, waarna u een "
"profilering-programma gebruikt (vaak meegeleverd met het apparaat)."

#: ../../general_concepts/colors/profiling_and_callibration.rst:60
msgid ""
"If you don't have software packaged with your colorimeter, or are unhappy "
"with the results, we recommend `ArgyllCMS <https://www.argyllcms.com/>`_."
msgstr ""
"Als er geen software met uw colorimeter wordt meegeleverd, of u bent "
"ontevreden over de resultaten, dan raden we `ArgyllCMS <https://www."
"argyllcms.com/>` aan_."

#: ../../general_concepts/colors/profiling_and_callibration.rst:62
msgid ""
"The little camera then measures what the brightest red, green, blue, white "
"and black are like on your screen using a predefined white as base. It also "
"measures how gray the color gray is."
msgstr ""
"De kleine camera meet dan hoe het helderste rood, groen, blauw, wit en zwart "
"op uw scherm eruit zien met een voorgedefinieerde wit als basis. Het meet "
"ook hoe grijs de kleur grijs is."

#: ../../general_concepts/colors/profiling_and_callibration.rst:64
msgid ""
"It then puts all this information into an ICC profile, which can be used by "
"the computer to correct your colors."
msgstr ""
"Het stopt dan al deze informatie in een ICC profiel, wat dan door de "
"computer gebruikt kan worden om uw kleuren te corrigeren."

#: ../../general_concepts/colors/profiling_and_callibration.rst:66
msgid ""
"It's recommended not to change the \"calibration\" (contrast, brightness, "
"you know the menu) of your screen after profiling. Doing so makes the "
"profile useless, as the qualities of the screen change significantly while "
"calibrating."
msgstr ""
"Het wordt aanbevolen om na het profileren de \"kalibratie\" (contrast, "
"helderheid, u weet het menu) van uw beeldscherm niet meer te wijzigen. Door "
"dit te doen wordt het profiel zinloos, omdat de kwaliteiten van het "
"beeldscherm nogal veranderen bij het kalibreren."

#: ../../general_concepts/colors/profiling_and_callibration.rst:68
msgid ""
"To make your screen display more accurate colors, you can do one or two "
"things: profile your screen or calibrate and profile it."
msgstr ""
"Om te zorgen dat uw beeldscherm de kleuren accurater weergeeft, kunt u een "
"van de volgende twee dingen doen: een profiel van uw beeldscherm maken of "
"het kalibreren en een profiel ervan maken."

#: ../../general_concepts/colors/profiling_and_callibration.rst:71
msgid ""
"Just profiling your screen means measuring the colors of your monitor with "
"its native settings and put those values in a color profile, which can be "
"used by color-managed application to adapt source colors to the screen for "
"optimal result. Calibrating and profiling means the same except that first "
"you try to calibrate the screen colors to match a certain standard setting "
"like sRGB or other more specific profiles. Calibrating is done first with "
"hardware controls (lightness, contrast, gamma curves), and then with "
"software that creates a vcgt (video card gamma table) to load in the GPU."
msgstr ""
"Alleen van uw beeldscherm een profiel maken houdt in dat van uw beeldscherm "
"met de standaard instellingen de kleuren worden gemeten en dat deze waarden "
"in een kleurprofiel worden geplaatst, dat dan door een programma met "
"kleurbeheer kan worden gebruikt om de bronkleuren aan te passen voor uw "
"beeldscherm voor een optimaal resultaat. Kalibreren en een profiel maken "
"houd hetzelfde in behalve dat u eerst probeert om de kleuren op het "
"beeldscherm overeen te laten komen met bepaalde standaarden zoals sRGB of "
"andere meer specifiekere profielen. Kalibratie wordt dan eerst gedaan met "
"hardware knoppen (Helderheid, contrast, gamma curven), en vervolgens met "
"software dat een vcgt (video card gamma tabel) creëert dat in de GPU wordt "
"geladen."

#: ../../general_concepts/colors/profiling_and_callibration.rst:75
msgid "So when or why should you do just one or both?"
msgstr "Wanneer en waarom zou u alleen het ene of juist het beide doen?"

#: ../../general_concepts/colors/profiling_and_callibration.rst:77
msgid "Profiling only:"
msgstr "Alleen een profiel maken:"

#: ../../general_concepts/colors/profiling_and_callibration.rst:79
msgid "With a good monitor"
msgstr "Met een goed beeldscherm"

#: ../../general_concepts/colors/profiling_and_callibration.rst:80
msgid ""
"You can get most of the sRGB colors and lot of extra colors not inside sRGB. "
"So this can be good to have more visible colors."
msgstr ""
"U kunt de meeste sRGB kleuren krijgen en veel extra kleuren niet in sRGB. "
"Dus dit is goed om meer zichtbare kleuren te hebben."

#: ../../general_concepts/colors/profiling_and_callibration.rst:82
msgid "With a bad monitor"
msgstr "Met een slecht beeldscherm"

#: ../../general_concepts/colors/profiling_and_callibration.rst:82
msgid ""
"You will get just a subset of actual sRGB, and miss lot of details, or even "
"have hue shifts. Trying to calibrate it before profiling can help to get "
"closer to full-sRGB colors."
msgstr ""
"U zal alleen een gedeelte van de eigenlijke sRGB krijgen, en veel details "
"missen, of zelfs kleur-verschuivingen hebben. Proberen te kalibreren voordat "
"u een profiel maakt kan helpen om dichterbij de volledige sRGB kleuren te "
"komen."

#: ../../general_concepts/colors/profiling_and_callibration.rst:84
msgid "Calibration+profiling:"
msgstr "Kalibreren+profiel maken:"

#: ../../general_concepts/colors/profiling_and_callibration.rst:86
msgid "Bad monitors"
msgstr "Slechte beeldschermen"

#: ../../general_concepts/colors/profiling_and_callibration.rst:87
msgid "As explained just before."
msgstr "Als eerder uitgelegd."

#: ../../general_concepts/colors/profiling_and_callibration.rst:88
msgid "Multi-monitor setup"
msgstr "Opstelling met meerdere beeldschermen"

#: ../../general_concepts/colors/profiling_and_callibration.rst:89
msgid ""
"When using several monitors, and specially in mirror mode where both monitor "
"have the same content, you can't have this content color-managed for both "
"screen profiles. In such case, calibrating both screens to match sRGB "
"profile (or another standard for high-end monitors if they both support it) "
"can be a good solution."
msgstr ""
"Bij het gebruik van meerdere beeldschermen, met name in mirror mode waar "
"beide beeldschermen hetzelfde laten zien, kunt u deze afbeelding niet kleur-"
"beheerd hebben voor beide beeldscherm-profielen. In dat geval, kan het "
"kalibreren van beide beeldschermen om ze met sRGB profiel (of een andere  "
"standaard voor high-end beeldschermen als ze beide dat ondersteunen) te "
"laten overeenkomen, een goede oplossing zijn."

#: ../../general_concepts/colors/profiling_and_callibration.rst:91
msgid ""
"When you need to match an exact rendering context for soft-proofing, "
"calibrating can help getting closer to the expected result. Though switching "
"through several monitor calibration and profiles should be done extremely "
"carefully."
msgstr ""
"Als het voor soft-proofing het nauwkeurig overeen moet komen, dan kan "
"kalibratie helpen om het beter met het verwachte resultaat overeen te laten "
"komen. Maar u moet heel voorzichtig zijn met het wisselen van meerdere "
"beeldscherm- kalibraties en -profielen."
