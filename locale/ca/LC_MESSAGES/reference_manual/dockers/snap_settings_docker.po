# Translation of docs_krita_org_reference_manual___dockers___snap_settings_docker.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 20:32+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../<generated>:1
msgid "Guides"
msgstr "Guies"

#: ../../reference_manual/dockers/snap_settings_docker.rst:1
msgid "Overview of the snap settings docker."
msgstr "Vista general de l'acoblador Ajustaments per ajustar."

#: ../../reference_manual/dockers/snap_settings_docker.rst:11
msgid "Snap"
msgstr "Ajustar"

#: ../../reference_manual/dockers/snap_settings_docker.rst:16
msgid "Snap Settings"
msgstr "Ajustaments per ajustar"

#: ../../reference_manual/dockers/snap_settings_docker.rst:20
msgid ""
"This docker has been removed in Krita 3.0. For more information on how to do "
"this instead, consult the :ref:`snapping page <snapping>`."
msgstr ""
"Aquest acoblador ha estat eliminat en el Krita 3.0. Per obtenir més "
"informació sobre com fer-ho, consulteu la :ref:`pàgina Ajustar <snapping>`."

#: ../../reference_manual/dockers/snap_settings_docker.rst:23
msgid ".. image:: images/dockers/Krita_Snap_Settings_Docker.png"
msgstr ".. image:: images/dockers/Krita_Snap_Settings_Docker.png"

#: ../../reference_manual/dockers/snap_settings_docker.rst:24
msgid ""
"This is docker only applies for Vector Layers. Snapping determines where a "
"vector shape will snap. The little number box is for snapping to a grid."
msgstr ""
"Aquest acoblador només s'aplica a les Capes vectorials. Ajustar determina on "
"es podrà ajustar una forma vectorial. El petit quadre numèric és per ajustar "
"amb una quadrícula."

#: ../../reference_manual/dockers/snap_settings_docker.rst:26
msgid "Node"
msgstr "Node"

#: ../../reference_manual/dockers/snap_settings_docker.rst:27
msgid "For snapping to other vector nodes."
msgstr "Per ajustar amb altres nodes vectorials."

#: ../../reference_manual/dockers/snap_settings_docker.rst:28
msgid "Extensions of Line"
msgstr "Extensions de la línia"

#: ../../reference_manual/dockers/snap_settings_docker.rst:29
msgid ""
"For snapping to a point that could have been part of a line, had it been "
"extended."
msgstr ""
"Per ajustar amb un punt que podria haver format part d'una línia, s'havia "
"estès."

#: ../../reference_manual/dockers/snap_settings_docker.rst:30
msgid "Bounding Box"
msgstr "Quadre contenidor"

#: ../../reference_manual/dockers/snap_settings_docker.rst:31
msgid "For snapping to the bounding box of a vector shape."
msgstr "Per ajustar amb el quadre contenidor d'una forma vectorial."

#: ../../reference_manual/dockers/snap_settings_docker.rst:32
msgid "Orthogonal"
msgstr "Ortogonal"

#: ../../reference_manual/dockers/snap_settings_docker.rst:33
msgid "For snapping to only horizontal or vertical lines."
msgstr "Per ajustar només amb les línies horitzontals o verticals."

#: ../../reference_manual/dockers/snap_settings_docker.rst:34
msgid "Intersection"
msgstr "Intersecció"

#: ../../reference_manual/dockers/snap_settings_docker.rst:35
msgid "For snapping to other vector lines."
msgstr "Per ajustar amb altres línies vectorials."

#: ../../reference_manual/dockers/snap_settings_docker.rst:37
msgid "Guides don't exist in Krita, therefore this one is useless."
msgstr "Les guies no existeixen al Krita, per tant aquesta és inútil."
