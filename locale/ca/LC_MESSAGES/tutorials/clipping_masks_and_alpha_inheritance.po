# Translation of docs_krita_org_tutorials___clipping_masks_and_alpha_inheritance.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-15 13:25+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Composition_animation.gif\n"
"   :alt: Animation showing that groups are composed before the rest of "
"composition takes place."
msgstr ""
".. image:: images/clipping-masks/Composition_animation.gif\n"
"   :alt: Animació que mostra que els grups s'han compost abans que tingui "
"lloc la resta de la composició."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/layers/Layer-composite.png\n"
"   :alt: An image showing the way layers composite in Krita"
msgstr ""
".. image:: images/layers/Layer-composite.png\n"
"   :alt: Una imatge que mostra la manera en què s'han compost les capes en "
"el Krita."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/layers/Krita-tutorial2-I.1-2.png\n"
"   :alt: An image showing how the alpha inheritance works and affects layers."
msgstr ""
".. image:: images/layers/Krita-tutorial2-I.1-2.png\n"
"   :alt: Una imatge que mostra com funcionen i afecten les capes a "
"l'herència alfa."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_1.png\n"
"   :alt: an image with line art and a layer for each flat of color"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_1.png\n"
"   :alt: Una imatge amb la línia artística i una capa per a cada pla de "
"color."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_2.png\n"
"   :alt: An image showing how the alpha inheritance works and affects layers."
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_2.png\n"
"   :alt: Una imatge que mostra com funcionen i afecten les capes a "
"l'herència alfa."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_3.png\n"
"   :alt: clipping mask step 3"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_3.png\n"
"   :alt: Màscares de retallat, pas 3."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_4.png\n"
"   :alt: clipping mask step 4"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_4.png\n"
"   :alt: Màscares de retallat, pas 4."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_5.png\n"
"   :alt: clipping mask step 5"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_5.png\n"
"   :alt: Màscares de retallat, pas 5."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_6.png\n"
"   :alt: clipping mask step 6"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_6.png\n"
"   :alt: Màscares de retallat, pas 6."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_7.png\n"
"   :alt: clipping mask step 7"
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_7.png\n"
"   :alt: Màscares de retallat, pas 7."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_clip_blur.gif\n"
"   :alt: filter layers and alpha inheritance"
msgstr ""
".. image:: images/clipping-masks/Tut_clip_blur.gif\n"
"   :alt: Capes de filtratge i l'herència alfa."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:1
msgid "An introduction for using clipping masks in Krita."
msgstr "Una introducció per utilitzar les màscares de retallat en el Krita."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:10
msgid "Alpha Inheritance"
msgstr "Herència alfa"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:10
msgid "Clipping Masks"
msgstr "Màscares de retallat"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:15
msgid "Clipping Masks and Alpha Inheritance"
msgstr "Màscares de retallat i herència alfa"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:17
msgid ""
"Krita doesn't have clipping mask functionality in the manner that Photoshop "
"and programs that mimic Photoshop's functionality have. That's because in "
"Krita, unlike such software, a group layer is not an arbitrary collection of "
"layers. Rather, in Krita, group layers are composited separately from the "
"rest of the stack, and then the result is added into the stack. In other "
"words, in Krita group layers are in effect a separate image inside your "
"image."
msgstr ""
"El Krita no té la funcionalitat de màscara de retallat de la manera que la "
"tenen el Photoshop i els programes que imiten la seva funcionalitat. Això és "
"perquè en el Krita, a diferència d'aquest programari, una capa de grup no és "
"una col·lecció arbitrària de capes. En canvi, en el Krita, les capes de grup "
"es componen per separat de la resta de la pila, i després el resultat "
"s'afegeix a la pila. En altres paraules, en el Krita les capes de grup són "
"en efecte una imatge separada dins de la vostra imatge."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:24
msgid ""
"The exception is when using pass-through mode, meaning that alpha "
"inheritance won't work right when turning on pass-through on the layer."
msgstr ""
"L'excepció és quan s'utilitza el mode curtcircuitat, el qual vol dir que "
"l'herència alfa no funcionarà correctament en activar el curtcircuitat a la "
"capa."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:30
msgid ""
"When we turn on alpha inheritance, the alpha-inherited layer keeps the same "
"transparency as the layers below."
msgstr ""
"Quan activem l'herència alfa, la capa heretada alfa mantindrà la mateixa "
"transparència que les capes de sota."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:36
msgid ""
"Combined with group layers this can be quite powerful. A situation where "
"this is particularly useful is the following:"
msgstr ""
"Combinada amb les capes de grup, això pot ser molt poderós. Una situació en "
"què això és particularment útil és la següent:"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:42
msgid ""
"Here we have an image with line art and a layer for each flat of colors. We "
"want to add complicated multi-layered shading to this, while keeping the "
"neatness of the existing color flats. To get a clipping mask working, you "
"first need to put layers into a group. You can do this by making a group "
"layer and drag-and-dropping the layers into it, or by selecting the layers "
"you want grouped and pressing the :kbd:`Ctrl + G` shortcut. Here we do that "
"with the iris and the eye-white layers."
msgstr ""
"Aquí tenim una imatge amb la línia artística i una capa per a cada pla de "
"colors. Li volem afegir un ombrejat complicat de múltiples capes, mantenint "
"la pulcritud dels plans de color existents. Perquè funcioni una màscara de "
"retallat, primer s'hauran de col·locar les capes en un grup. Podeu fer-ho "
"creant una capa de grup i arrossegant i deixant anar les capes a sobre seu, "
"o seleccionant les capes que voleu agrupar i prement la drecera :kbd:`Ctrl + "
"G`. Aquí ho fem amb les capes amb l'iris i el blanc dels ulls."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:50
msgid ""
"We add a layer for the highlight above the other two layers, and add some "
"white scribbles."
msgstr ""
"Afegim una capa per a les llums intenses a sobre de les altres dues capes, i "
"afegim alguns gargots en blanc."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:60
msgid ""
"In the above, we have our layer with a white scribble on the left, and on "
"the right, the same layer, but with alpha inheritance active, limiting it to "
"the combined area of the iris and eye-white layers."
msgstr ""
"En l'anterior, tenim la nostra capa amb un gargot en blanc a l'esquerra, i a "
"la dreta, la mateixa capa, però amb l'herència alfa activada, limitant-la a "
"l'àrea combinada de les capes amb l'iris i el blanc dels ulls."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:66
msgid ""
"Now there’s an easier way to set up alpha inheritance. If you select a layer "
"or set of layers and press the :kbd:`Ctrl + Shift + G` shortcut, you create "
"a quick clipping group. That is, you group the layers, and a ‘mask layer’ "
"set with alpha inheritance is added on top."
msgstr ""
"Hi ha una manera més fàcil de configurar l'herència alfa. Si seleccioneu una "
"capa o conjunt de capes i premeu la drecera :kbd:`Ctrl + Majús. + G`, es "
"crearà ràpidament un grup de retallat. És a dir, s'agruparan les capes i "
"s'afegirà una «capa de màscara» amb l'herència alfa a la part superior."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:76
msgid ""
"The fact that alpha inheritance can use the composited transparency from a "
"combination of layers means that you can have a layer with the erase-"
"blending mode in between, and have that affect the area that the layer above "
"is clipped to. Above, the lower image is exactly the same as the upper one, "
"except with the erase-layer hidden. Filters can also affect the alpha "
"inheritance:"
msgstr ""
"El fet que l'herència alfa pugui utilitzar la transparència composta des "
"d'una combinació de les capes vol dir que podreu tenir una capa amb el mode "
"de barreja esborrat entremig, i que això afecti l'àrea en la qual es retalla "
"la capa de dalt. A dalt, la imatge de sota és exactament la mateixa que la "
"de sobre, excepte amb la capa d'esborrat oculta. Els filtres també poden "
"afectar l'herència alfa:"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:83
msgid ""
"Above, the blur filter layer gives different results when in different "
"places, due to different parts being blurred."
msgstr ""
"A dalt, la capa de filtratge amb difuminat proporciona resultats diferents "
"quan es troba en diferents llocs, pel fet que es difuminen diferents parts."
