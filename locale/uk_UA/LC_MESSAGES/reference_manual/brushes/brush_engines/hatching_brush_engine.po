# Translation of docs_krita_org_reference_manual___brushes___brush_engines___hatching_brush_engine.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___hatching_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:29+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/brushes/Krita-tutorial8-A.II.png"
msgstr ".. image:: images/brushes/Krita-tutorial8-A.II.png"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:1
msgid "The Hatching Brush Engine manual page."
msgstr "Розділ підручника щодо рушія штрихових пензлів."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:17
msgid "Hatching Brush Engine"
msgstr "Рушій штрихового пензля"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:12
msgid "Brush Engine"
msgstr "Рушій пензлів"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:12
msgid "Hatching"
msgstr "Штрихування"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:12
msgid "Halftone"
msgstr "Півтон"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:21
msgid ".. image:: images/icons/hatchingbrush.svg"
msgstr ".. image:: images/icons/hatchingbrush.svg"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:22
msgid ""
"When I first tried this brush, my impression of it was \"plain parallel lines"
"\" (and the award for most boring brush goes to...). Fortunately, existing "
"presets gave me an idea of the possibilities of this brush."
msgstr ""
"Коли автор уперше спробував цей пензель, враження було «прості паралельні "
"лінії» (і нагороду за найнудніший пензель буде вручено…). На щастя, за "
"допомогою наявних наборів пензлів автору вдалося розібратися із усіма "
"можливостями цього пензля."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:25
msgid "Settings"
msgstr "Параметри"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:28
msgid "Brush tip"
msgstr "Кінчик пензля"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:30
msgid ""
"The brush tip simply defines the area where the hatching will be rendered."
msgstr "Кінчик пензля просто визначає ділянку, де буде показано штрихування."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:32
msgid ""
"Transparent brush tip areas give more transparent hatching, but as with a "
"normal brush, passing over the area again will increase opacity."
msgstr ""
"Прозорі ділянки кінчика пензля дають прозоріше штрихування, але, як і зі "
"звичайним пензлем, повторне малювання на ділянці збільшує рівень "
"непрозорості."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:33
msgid ""
"The hatching itself is mostly fixed in location, so drawing with a hatching "
"brush usually acts more like \"revealing\" the hatching underneath than "
"drawing with brushes of parallel lines. The exception is for :guilabel:"
"`Moiré pattern` with :guilabel:`Crosshatching` dynamics on."
msgstr ""
"Саме штрихування здебільшого є нерухомим, тому малювання за допомогою пензля "
"штрихування насправді працює як «проявлення» штрихування, а не малювання за "
"допомогою пензлів із паралельними штрихами. Винятком є :guilabel:`муар` із "
"увімкненою динамікою :guilabel:`Поперечне штрихування`."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:34
msgid ""
"Vary the brush shape or texture for a variety of effects. Decreasing the "
"density of the autobrush will give a grainy texture to your hatching, for "
"example."
msgstr ""
"Змінюйте форму або текстуру пензля для отримання спектру ефектів. Наприклад, "
"зменшення щільності автоматичного пензля надасть штрихуванню зернистої "
"текстури."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:35
msgid ""
"The :guilabel:`Size` dynamic affects the brush tip, not the hatching "
"thickness."
msgstr ""
"Динаміка :guilabel:`Розмір` впливає на кінчик пензля, а не на товщину ліній "
"штрихування."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:38
msgid ".. image:: images/brushes/Krita-tutorial8-A.I.1.png"
msgstr ".. image:: images/brushes/Krita-tutorial8-A.I.1.png"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:40
msgid "Hatching preferences"
msgstr "Налаштування штрихування"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:42
msgid ""
"Before going on: at the time of this writing, there is a bug that causes "
"line thickness to not vary on default settings. To get around this, go to :"
"guilabel:`Hatching preferences` and check :guilabel:`Antialiased Lines`. "
"Pentalis is aware of this issue so the bug may get fixed soon."
msgstr ""
"Перш ніж продовжувати, на час написання цього розділу у програмі була вада, "
"яка спричиняла незмінність товщини ліній при типових налаштуваннях. Щоб "
"усунути цю проблему, перейдіть до пункту :guilabel:`Налаштування "
"штрихування` і позначте :guilabel:`Згладжені лінії`. Розробники знають про "
"цю проблему, отже невдовзі ваду може бути виправлено."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:44
msgid "The three options are:"
msgstr "Трьома параметрами є:"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:46
msgid ""
":guilabel:`Antialiased lines`: This controls aliasing. If changing line "
"thickness isn't working, check this option and it should work, because it "
"switches to a different algorithm."
msgstr ""
":guilabel:`Згладжені лінії`: цей пункт керує згладжуванням. Якщо не працює "
"зміна товщини ліній, позначте цей пункт, і все має запрацювати, оскільки "
"програма перемкнеться на інший алгоритм обчислення товщини."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:47
msgid ""
":guilabel:`Subpixel precision`: I'm guessing this affects the rendering "
"quality, but you won't see much of a difference. Check this if you want to."
msgstr ""
":guilabel:`Субпіксельна точність`: здається, цей пункт стосується якості "
"обробки зображення, але різниця не дуже помітна. Позначте його, якщо хочете."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:48
msgid ""
":guilabel:`Color background`: Checking this will color in the background at "
"the back of the hatching."
msgstr ""
":guilabel:`Розфарбувати тло`: якщо буде позначено цей пункт, програма "
"розфарбує тло під штрихуванням."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:50
msgid ""
"The output is slightly different depending on whether the first two options "
"are checked, but the difference isn't enough for you to worry about. I "
"recommend just keeping the first two options checked."
msgstr ""
"Результат дещо залежить від того, чи позначено перші два пункти, але різниця "
"не така помітна, щоб нею перейматися. Рекомендуємо просто позначити перші "
"два пункти."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:53
msgid ".. image:: images/brushes/Krita-tutorial8-A.I.2.png"
msgstr ".. image:: images/brushes/Krita-tutorial8-A.I.2.png"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:55
msgid "Hatching options"
msgstr "Параметри штрихування"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:57
msgid "This is where the main hatching options go. They're intuitive enough:"
msgstr ""
"Тут наведено пункти основних параметрів штрихування. Їхнє призначення доволі "
"інтуїтивно зрозуміле:"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:59
msgid "Angle: The angle of the hatching."
msgstr "Кут: кут нахилу штрихування."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:60
msgid "Separation: This is the distance between the centers of the lines."
msgstr "Інтервал: відстань між лініями у штрихуванні."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:62
msgid ""
"Use a value of 2 pixels or higher, or the lines won't be distinct anymore."
msgstr ""
"Скористайтеся значенням у 2 пікселі або більшим, інакше лінії буде важко "
"розрізнити."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:63
msgid ""
"The Separations dynamic doesn't actually assign random values to Separation, "
"instead it will take the value in \"Input-based intervals\" to divide the "
"grid further. \"Input-based intervals\" can take values between 2 and 7."
msgstr ""
"Динаміка «Інтервал» насправді не встановлює випадкові значення для "
"інтервалу, а використовує значення «Інтервали на основі вхідних даних» для "
"подальшого поділу ґратки. Значенням параметра «Інтервали на основі вхідних "
"даних» може бути число від 2 до 7."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:65
msgid "Thickness: The line thickness."
msgstr "Товщина: товщина лінії."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:67
msgid ""
"Actually, this is the thickness of the line + blank area, so the line itself "
"has a thickness of half this value."
msgstr ""
"Насправді, це товщина лінії + порожньої ділянки, отже сама лінія буде удвічі "
"тонша за це значення."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:68
msgid ""
"If you use the same separation value and the same line thickness value, then "
"the lines and the area between them will be of the same thickness."
msgstr ""
"Якщо ви використовуєте однакове значення для інтервалу і товщини лінії, "
"лінії і області між ними будуть однакової товщини."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:69
msgid "You can vary this value dynamically with the Thickness dynamics."
msgstr ""
"Ви можете динамічно змінювати це значення за допомогою динаміки товщини."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:70
msgid ""
"If the line thickness isn't changing for you, go to Hatching Preferences and "
"check \"Antialiased Lines\"."
msgstr ""
"Якщо товщина лінії не змінюватиметься, скористайтеся «Налаштуваннями "
"штрихування»: позначте пункт «Згладжені лінії»."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:72
msgid ""
"Origin X and Origin Y: The hatching has a fixed location, painting acts as "
"though you're revealing the existing hatching underneath. To nudge the "
"hatching, you can tweak these two values. You can get various grid effects "
"this way."
msgstr ""
"Початок за X і Початок за Y: штрихування має фіксовану позицію — малювання "
"просто «проявляє» наявне під ним штрихування. Щоб посунути штрихування ви "
"можете скористатися цими двома значеннями. За допомогою цих параметрів можна "
"отримувати різні ефекти ґратки."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:75
msgid ".. image:: images/brushes/Krita-tutorial8-A.I.3-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial8-A.I.3-1.png"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:76
msgid "Finally, we have the hatching styles:"
msgstr "Нарешті, у нашому розпорядженні стилі штрихування:"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:78
msgid "No crosshatching: basic parallel lines"
msgstr "Без поперечного штрихування: базові паралельні лінії"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:79
msgid "Perpendicular plane only: grid lines"
msgstr "Лише у перпендикулярній площині: лінії ґратки"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:80
msgid "-45 degrees plane then +45 degrees plane: see example."
msgstr "Площина -45 градусів, потім площина +45 градусів — див. приклад."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:81
msgid ""
"+45 degrees plane then -45 degrees plane: see example, actually not much "
"different from the above, it's mostly the order that changes when using "
"dynamics."
msgstr ""
"Площина +45 градусів, потім площина -45 градусів — див. приклад. Насправді, "
"не дуже відрізняється від прикладу вище, — це здебільшого порядок, який "
"змінюється при використанні динаміки."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:82
msgid "Moiré pattern: See example."
msgstr "Муар: див. приклад."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:84
msgid ""
"The Crosshatching dynamic only works if you have already chosen a "
"crosshatching style. When that happens, the crosshatching only gets drawn "
"according to the conditions of the dynamics (pressure, speed, angle...)."
msgstr ""
"Динаміка поперечного штрихування працює, лише якщо ви вже вибрали стиль "
"поперечного штрихування. Якщо налаштовано, програма малюватиме поперечне "
"штрихування за умовами динаміки (тиску, швидкості, кута...)."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:86
msgid ""
"With most hatching styles, using crosshatching dynamics basically gets you "
"the same hatching style, minus the occasional line."
msgstr ""
"З більшістю стилів штрихування використання динаміки поперечного штрихування "
"на базовому рівні дає той самий стиль штрихування, можливо, без якоїсь "
"рідкісної лінії."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:87
msgid "The exception is with Moire, which will produce a different pattern."
msgstr "Виключенням є муар, який створює інший візерунок."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:90
msgid ".. image:: images/brushes/Krita-tutorial8-A.I.3-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial8-A.I.3-2.png"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:92
msgid "Use cases"
msgstr "Користування"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:94
msgid ""
"If you don't want the edges to be fuzzy, go to Brush Tip and set the Fade "
"values to 1.00. I recommended doing the hatching on a separate layer, then "
"erasing the extra areas."
msgstr ""
"Якщо вам не хочеться, щоб краї були нечіткими, перейдіть до панелі "
"параметрів кінчика пензля і встановіть значення згасання 1.00. Рекомендуємо "
"виконувати штрихування на окремому шарі, а потім витерти зайві ділянки."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:96
msgid "Now for the uses:"
msgstr "Тепер поради щодо користування:"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:98
msgid ""
"You can, of course, just use this for completely normal hatching. In "
"versions I'm using, the default Separation is 1, which is too low, so "
"increase Separation to a value between 2 to 10."
msgstr ""
"Ви, звичайно ж, можете користуватися пензлем для звичайного штрихування. У "
"використаних автором підручника версіях типовий інтервал дорівнював 1, а це "
"надто мало, отже збільште значення інтервалу до числа від 2 до 10."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:99
msgid ""
"If you find normal hatching too boring, increase the Thickness and set the "
"Thickness dynamic to either Pressure (if you have a tablet) or Speed (if "
"you're using a mouse). Doesn't that look more natural? (When using a mouse, "
"pass over the areas where you want thicker lines again while drawing faster)."
msgstr ""
"Якщо звичайне штрихування здається вам надто правильним, збільште товщину і "
"встановіть динаміку товщини у значення :guilabel:`Тиск` (якщо ви "
"користуєтеся планшетом) або :guilabel:`Швидкість` (якщо ви користуєтеся "
"мишею). Правда ж, виглядає природніше? (Якщо користуєтеся мишею, пройдіться "
"ділянками, де лінії штрихування мають бути товщими, малюючи швидше.)"

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:100
msgid ""
"Grittier texture: add some density and/or randomness to your autobrush for a "
"grittier texture."
msgstr ""
"Пісочна текстура: додайте щільності і/або випадковості до вашого автопензля, "
"щоб отримати пісочну структуру."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:101
msgid ""
"You can also set Painting Mode to Build up, and Mode to Multiply, to make "
"some colors have more depth. (see my grid example)."
msgstr ""
"Ви також можете встановити режим малювання у значення «Надбудова», а режим у "
"значення «Множення», щоб додати деяким кольорам глибини (див. приклад із "
"ґраткою)."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:102
msgid "Vary Origin X and Origin Y while using the same patterns."
msgstr ""
"Спробуйте позмінювати :guilabel:`Початок за X` і :guilabel:`Початок за Y`, "
"використовуючи той самий візерунок."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:103
msgid ""
"Use the Separations dynamic for more complex patterns. Add in Line Thickness "
"and other dynamics for more effect."
msgstr ""
"Для отримання складніших візерунків скористайтеся динамікою :guilabel:"
"`Інтервал`. Додайте динаміку товщини лінії та інші динаміки, щоб підсилити "
"ефект."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:104
msgid ""
"Now, the Moiré pattern is quite boring on its own, but it is much more "
"interesting with Crosshatching dynamics set on Fuzzy."
msgstr ""
"Нарешті, муар доволі неприродний без додаткових ефектів. Набагато цікавішим "
"його може зробити встановлення для динаміки перехресного штрихування "
"значення :guilabel:`Неточна`."

#: ../../reference_manual/brushes/brush_engines/hatching_brush_engine.rst:105
msgid ""
"For more texture, set Line Thickness to Fuzzy, decrease Density a bit and "
"increase Randomness and you get a nice gritty texture."
msgstr ""
"Щоб отримати красивішу текстуру, встановіть для товщини лінії динаміку :"
"guilabel:`Неточна`, дещо зменшіть щільність і збільште :guilabel:"
"`Випадковість`, і отримаєте чудову пісочну текстуру."
