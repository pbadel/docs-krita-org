# Translation of docs_krita_org_reference_manual___dockers___artistic_color_selector.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___artistic_color_selector\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:20+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/dockers/artistic_color_selector.rst:1
msgid "Overview of the artistic color selector docker."
msgstr "Огляд бічної панелі художнього вибору кольору."

#: ../../reference_manual/dockers/artistic_color_selector.rst:12
msgid "Color"
msgstr "Колір"

#: ../../reference_manual/dockers/artistic_color_selector.rst:12
msgid "Color Selector"
msgstr "Вибір кольору"

#: ../../reference_manual/dockers/artistic_color_selector.rst:12
msgid "Artistic Color Selector"
msgstr "Художній вибір кольору"

#: ../../reference_manual/dockers/artistic_color_selector.rst:17
msgid "Artistic Color Selector Docker"
msgstr "Бічна панель художнього вибору кольору"

#: ../../reference_manual/dockers/artistic_color_selector.rst:19
msgid "A color selector inspired by traditional color wheel and workflows."
msgstr ""
"Засіб вибору на основі традиційного кола кольорів та традиційних робочих "
"процесів."

#: ../../reference_manual/dockers/artistic_color_selector.rst:22
msgid "Usage"
msgstr "Користування"

#: ../../reference_manual/dockers/artistic_color_selector.rst:26
msgid ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker.png"
msgstr ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:26
msgid "Artistic color selector with a gamut mask."
msgstr "Художній вибір кольору зі маскою діапазону кольорів."

#: ../../reference_manual/dockers/artistic_color_selector.rst:28
msgid ""
"Select hue and saturation on the wheel (5) and value on the value scale (4). "
"|mouseleft| changes foreground color (6). |mouseright| changes background "
"color (7)."
msgstr ""
"Виберіть відтінок і насиченість на колі (5), а значення на шкалі значення "
"(4). Клацання |mouseleft| змінює колір переднього плану (6). Клацання |"
"mouseright| змінює колір тла (7)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:30
msgid ""
"The blip shows the position of current foreground color on the wheel "
"(black&white circle) and on the value scale (black&white line). Last "
"selected swatches are outlined."
msgstr ""
"Позначка вказуватиме розташування поточного кольору переднього плану на колі "
"(чорно-білий кружечок) та на шкалі значення (чорно-біла лінія). Останні "
"вибрані плашки буде позначено рамкою."

#: ../../reference_manual/dockers/artistic_color_selector.rst:32
msgid ""
"Parameters of the wheel can be set in :ref:"
"`artistic_color_selector_docker_wheel_preferences` menu (2). Selector "
"settings are found under :ref:"
"`artistic_color_selector_docker_selector_settings` menu (3)."
msgstr ""
"Параметри кола можна визначити у меню :ref:"
"`artistic_color_selector_docker_wheel_preferences` (2). Доступ до параметрів "
"засобу вибору кольору здійснюється за допомогою меню :ref:"
"`artistic_color_selector_docker_selector_settings` (3)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:35
msgid "Gamut Masking"
msgstr "Маскування діапазону кольорів"

#: ../../reference_manual/dockers/artistic_color_selector.rst:37
msgid ""
"You can select and manage your gamut masks in the :ref:`gamut_mask_docker`."
msgstr ""
"Вибрати маску діапазону кольорів та керувати масками діапазонів кольорів "
"можна за допомогою :ref:`бічної панелі маски діапазону кольорів "
"<gamut_mask_docker>`."

#: ../../reference_manual/dockers/artistic_color_selector.rst:39
msgid ""
"In the gamut masking toolbar (1) you can toggle the selected mask off and on "
"(left button). You can also rotate the mask with the rotation slider (right)."
msgstr ""
"За допомогою панелі інструментів маскування діапазону кольорів (1) ви можете "
"вмикати і вимикати вибрану маску (ліва кнопка). Ви також можете обертати "
"маску за допомогою повзунка обертання (праворуч)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:45
msgid "Color wheel preferences"
msgstr "Параметри кругу кольорів"

#: ../../reference_manual/dockers/artistic_color_selector.rst:50
msgid ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_3.png"
msgstr ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_3.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:50
msgid "Color wheel preferences."
msgstr "Параметри кругу кольорів."

#: ../../reference_manual/dockers/artistic_color_selector.rst:53
msgid "Sliders 1, 2, and 3"
msgstr "Повзунки 1, 2 і 3"

#: ../../reference_manual/dockers/artistic_color_selector.rst:53
msgid ""
"Adjust the number of steps of the value scale, number of hue sectors and "
"saturation rings on the wheel, respectively."
msgstr ""
"Коригують кількість кроків на шкалі значення, кількість секторів відтінків "
"та кілець насиченості на колі, відповідно."

#: ../../reference_manual/dockers/artistic_color_selector.rst:56
msgid "Continuous Mode"
msgstr "Неперервний режим"

#: ../../reference_manual/dockers/artistic_color_selector.rst:56
msgid ""
"The value scale and hue sectors can also be set to continuous mode (with the "
"infinity icon on the right of the slider). If toggled on, the respective "
"area shows a continuous gradient instead of the discrete swatches."
msgstr ""
"Для шкали значення і секторів відтінку також можна встановити неперервний "
"режим (за допомогою піктограми нескінченності праворуч від повзунка). Якщо "
"увімкнути цей режим, у відповідній області буде показано неперервний "
"градієнт замість дискретних плашок."

#: ../../reference_manual/dockers/artistic_color_selector.rst:59
msgid "Invert saturation (4)"
msgstr "Інвертувати насиченість (4)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:59
msgid ""
"Changes the order of saturation rings within the the hue sectors. By "
"default, the wheel has gray in the center and most saturated colors on the "
"perimeter. :guilabel:`Invert saturation` puts gray on the perimeter and most "
"saturated colors in the center."
msgstr ""
"Змінює порядок кілець насиченості у секторах відтінків. Типово, у центрі "
"круга розташовуватиметься сірий, а більшість насичених кольорів "
"розташовуватиметься на периметрі. Позначення пункту :guilabel:`Інвертувати "
"насиченість` призведе до того, що сірий розташується на периметрі, а "
"насиченіші кольори — у центрі."

#: ../../reference_manual/dockers/artistic_color_selector.rst:62
msgid ""
"Loads default values for the sliders 1, 2, and 3. These default values are "
"configured in selector settings."
msgstr ""
"Завантажує типові значення для повзунків 1, 2 і 3. Ці типові значення "
"налаштовано у параметрах засобу вибору кольору."

#: ../../reference_manual/dockers/artistic_color_selector.rst:63
msgid "Reset to default (5)"
msgstr "Повернутися до типового (5)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:68
msgid "Selector settings"
msgstr "Параметри засобу вибору"

#: ../../reference_manual/dockers/artistic_color_selector.rst:72
msgid ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Artistic_Color_Selector_Docker_2.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:72
msgid "Selector settings menu."
msgstr "Меню параметрів засобу вибору."

#: ../../reference_manual/dockers/artistic_color_selector.rst:75
msgid "Show background color indicator"
msgstr "Показувати індикатор кольору тла"

#: ../../reference_manual/dockers/artistic_color_selector.rst:76
msgid "Toggles the bottom-right triangle with current background color."
msgstr ""
"Вмикає або вимикає показ поточного кольору тла у правому нижньому трикутнику "
"панелі засобу вибору кольору."

#: ../../reference_manual/dockers/artistic_color_selector.rst:78
msgid "Selector Appearance (1)"
msgstr "Вигляд засобу вибору (1)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:78
msgid "Show numbered value scale"
msgstr "Показувати нумеровану шкалу значень"

#: ../../reference_manual/dockers/artistic_color_selector.rst:78
msgid ""
"If checked, the value scale includes a comparative gray scale with lightness "
"percentage."
msgstr ""
"Якщо позначено, до шкали значення буде включено порівняльну шкалу сірого "
"кольору із відсотковим значенням освітленості."

#: ../../reference_manual/dockers/artistic_color_selector.rst:81
msgid ""
"Set the color model used by the selector. For detailed information on color "
"models, see :ref:`color_models`."
msgstr ""
"Визначає модель кольорів, яку буде використано для засобу вибору. Докладні "
"відомості про моделі кольорів наведено у розділі :ref:`color_models`."

#: ../../reference_manual/dockers/artistic_color_selector.rst:84
msgid "Color Space (2)"
msgstr "Простір кольорів (2)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:84
msgid "Luma Coefficients (3)"
msgstr "Коефіцієнти яскравості (3)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:84
msgid ""
"If the selector's color space is HSY, you can set custom Luma coefficients "
"and the amount of gamma correction applied to the value scale (set to 1.0 "
"for linear scale; see :ref:`linear_and_gamma`)."
msgstr ""
"Якщо простором кольорів засобу вибору кольору є HSY, ви можете встановити "
"нетипові коефіцієнти яскравості кольору та величну виправлення гамми, яку "
"буде застосовано до шкали значення (встановіть 1.0, щоб отримати лінійну "
"шкалу; див. :ref:`linear_and_gamma`)."

#: ../../reference_manual/dockers/artistic_color_selector.rst:87
msgid "Gamut Masking Behavior (4)"
msgstr "Поведінка маскування діапазону кольорів (4)"

#: ../../reference_manual/dockers/artistic_color_selector.rst:87
msgid ""
"The selector can be set either to :guilabel:`Enforce gamut mask`, so that "
"colors outside the mask cannot be selected, or to :guilabel:`Just show the "
"shapes`, where the mask is visible but color selection is not limited."
msgstr ""
"Для засобу вибору кольору можна встановити або :guilabel:`Примусову маску "
"діапазону кольорів`, щоб кольори поза маскою не можна було вибрати, або :"
"guilabel:`Показувати лише форми`, коли маска буде видимою, але вибір "
"кольорів не обмежуватиметься."

#: ../../reference_manual/dockers/artistic_color_selector.rst:90
msgid "Default Selector Steps Settings"
msgstr "Типові параметри кроків засобу вибору"

#: ../../reference_manual/dockers/artistic_color_selector.rst:90
msgid ""
"Values the color wheel and value scale will be reset to default when the :"
"guilabel:`Reset to default` button in :ref:"
"`artistic_color_selector_docker_wheel_preferences` is pressed."
msgstr ""
"Значення на колі кольорів та значення на шкалі буде скинуто до типового, "
"коли буде натиснуто кнопку :guilabel:`Повернутися до типового` на панелі :"
"ref:`artistic_color_selector_docker_wheel_preferences`."

#: ../../reference_manual/dockers/artistic_color_selector.rst:93
msgid "External Info"
msgstr "Сторонні відомості"

#: ../../reference_manual/dockers/artistic_color_selector.rst:94
msgid ""
"`HSI and HSY for Krita’s advanced colour selector by Wolthera van Hövell tot "
"Westerflier. <https://wolthera.info/?p=726>`_"
msgstr ""
"`HSI і HSY для розширеного засобу вибору кольорів Krita, автор — Wolthera "
"van Hövell tot Westerflier <https://wolthera.info/?p=726>`_"

#: ../../reference_manual/dockers/artistic_color_selector.rst:95
msgid ""
"`The Color Wheel, Part 7 by James Gurney. <https://gurneyjourney.blogspot."
"com/2010/02/color-wheel-part-7.html>`_"
msgstr ""
"`Коло кольорів, частина 7, Джеймс Гурні (James Gurney) <https://"
"gurneyjourney.blogspot.com/2010/02/color-wheel-part-7.html>`_"

#~ msgid ""
#~ "A round selector that tries to give you the tools to select colors ramps "
#~ "efficiently."
#~ msgstr ""
#~ "Круглий засіб вибору кольору, у якому реалізовано спробу створення "
#~ "ефективного інструмента вибору кольорів."

#~ msgid "Preference"
#~ msgstr "Пріоритет"

#~ msgid "Reset"
#~ msgstr "Скинути"

#~ msgid "Reset the selector to a default stage."
#~ msgstr "Відновити типовий стан засобу вибору."

#~ msgid "Absolute"
#~ msgstr "Абсолютний"

#~ msgid ""
#~ "This changes the algorithm around so it gives proper values for the gray. "
#~ "Without absolute, it'll use HSV values for gray to the corresponding hue "
#~ "and lightness."
#~ msgstr ""
#~ "Змінює алгоритм для отримання належних значень сірого кольору. Без "
#~ "позначення пункту «Абсолютний» програма використовує значення HSV для "
#~ "сірого для відповідного відтінку та освітленості."

#~ msgid ""
#~ "|mouseleft| the swatches to change the foreground color. Use |mouseright| "
#~ "+ Drag to shift the alignment of the selector swatches within a specific "
#~ "saturation ring. Use |mouseleft| + Drag to shift the alignment of all "
#~ "swatches."
#~ msgstr ""
#~ "Клацніть |mouseleft| на елементі набору для зміни кольору переднього "
#~ "плану. Скористайтеся комбінацією |mouseright| + перетягування для "
#~ "посування вирівнювання елементів набору у певному кільці насиченості. "
#~ "Скористайтеся комбінацією |mouseleft| + перетягування для посування "
#~ "вирівнювання усіх елементів набору."

#~ msgid "This selector does not update on change of foreground color."
#~ msgstr ""
#~ "Вміст цього засобу вибору не оновлюється, якщо змінено колір переднього "
#~ "плану."
