# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:17+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/tools/crop.rst:1
msgid "Krita's crop tool reference."
msgstr "Referens för Kritas beskärningsverktyg."

#: ../../reference_manual/tools/crop.rst:11
msgid "Tools"
msgstr "Verktyg"

#: ../../reference_manual/tools/crop.rst:11
msgid "Crop"
msgstr "Beskär"

#: ../../reference_manual/tools/crop.rst:11
msgid "Trim"
msgstr "Beskär"

#: ../../reference_manual/tools/crop.rst:16
msgid "Crop Tool"
msgstr "Beskärningsverktyg"

#: ../../reference_manual/tools/crop.rst:18
msgid ""
"The crop tool can be used to crop an image or layer. To get started, choose "
"the :guilabel:`Crop` tool and then click once to select the entire canvas. "
"Using this method you ensure that you don't inadvertently grab outside of "
"the visible canvas as part of the crop. You can then use the options below "
"to refine your crop. Press the :kbd:`Enter` key to finalize the crop action, "
"or use the :guilabel:`Crop` button in the tool options docker."
msgstr ""
"Beskärningsverktyget kan användas för att beskära en bild eller ett lager. "
"För att komma igång, välj verktyget :guilabel:`Beskär` och klicka en gång "
"för att markera hela duken. Använd den här metoden för att säkerställa att "
"du inte tar tag utanför den synliga duken som en del av beskärningen. "
"Därefter kan alternativen nedanför användas för att förfina beskärningen. "
"Tryck på tangenten :kbd:`Enter` för att slutföra beskärningsåtgärden, eller "
"använd knappen :guilabel:`Beskär` i verktygsalternativpanelen."

#: ../../reference_manual/tools/crop.rst:20
msgid ""
"At its most basic, the crop tool allows you to size a rectangle around an "
"area and reduce your image or layer to only that content which is contained "
"within that area. There are several options which give a bit more "
"flexibility and precision."
msgstr ""
"Som mest grundläggande låter beskärningsverktyget dig skapa en rektangel av "
"en viss storlek omkring ett område och reducera bilden eller lagret till "
"enbart innehållet som finns inne i det området. Det finns flera alternativ "
"som ger lite mer flexibilitet och precision."

#: ../../reference_manual/tools/crop.rst:22
msgid ""
"The two numbers on the left are the exact horizontal position and vertical "
"position of the left and top of the cropping frame respectively. The numbers "
"are the right are from top to bottom: width, height, and aspect ratio. "
"Selecting the check boxes will keep any one of these can be locked to allow "
"you to manipulate the other two without losing the position or ratio of the "
"locked property."
msgstr ""
"De två talen till vänster är den exakta horisontella och vertikala "
"positionen för vänstra och övre hörnet av beskärningsrutan. Talen till höger "
"uppifrån och ner är: bredd, höjd och proportion. Att markera kryssrutorna "
"gör att vilken som helst av dem kan låsas för att låta dig manipulera de två "
"andra utan att förlora positionen eller förhållandet för den låsta "
"egenskapen."

#: ../../reference_manual/tools/crop.rst:24
msgid "Center"
msgstr "Centrera"

#: ../../reference_manual/tools/crop.rst:25
msgid "Keeps the crop area centered."
msgstr "Håller beskärningsområdet centrerat."

#: ../../reference_manual/tools/crop.rst:26
msgid "Grow"
msgstr "Öka"

#: ../../reference_manual/tools/crop.rst:27
msgid "Allows the crop area to expand beyond the image boundaries."
msgstr "Låter beskärningsområdet expanderas utanför bildgränserna."

#: ../../reference_manual/tools/crop.rst:28
msgid "Applies to"
msgstr "Gäller"

#: ../../reference_manual/tools/crop.rst:29
msgid ""
"Lets you apply the crop to the entire image or only to the active layer. "
"When you are ready, hit the :guilabel:`Crop` button and the crop will apply "
"to your image."
msgstr ""
"Låter dig utföra beskärning av hela bilden eller bara det aktiva lagret. När "
"du är klar, klicka på knappen :guilabel:`Beskär` så utförs beskärningen av "
"bilden."

#: ../../reference_manual/tools/crop.rst:31
msgid "Decoration"
msgstr "Dekoration"

#: ../../reference_manual/tools/crop.rst:31
msgid ""
"Help you make a composition by showing you lines that divide up the screen. "
"You can for example show thirds here, so you can crop your image according "
"to the `Rule of Thirds <https://en.wikipedia.org/wiki/Rule_of_thirds>`_."
msgstr ""
"Hjälper till att skapa en komposition genom att visa linjer som delar upp "
"skärmen. Man kan exempelvis visa tredjedelar här, så att bilden kan beskäras "
"enligt tredjedelsregeln <https://en.wikipedia.org/wiki/Rule_of_thirds>`_."

#: ../../reference_manual/tools/crop.rst:34
msgid "Continuous Crop"
msgstr "Kontinuerlig beskärning"

#: ../../reference_manual/tools/crop.rst:36
msgid ""
"If you crop an image, and try to start a new one directly afterwards, Krita "
"will attempt to recall the previous crop, so you can continue it. This is "
"the *continuous crop*. You can press the :kbd:`Esc` key to cancel this and "
"crop anew."
msgstr ""
"Om en bild beskärs, och man försöker påbörja en ny direkt efteråt, försöker "
"Krita återkalla föregående beskärning så att du kan fortsätta med den. Det "
"kallas *kontinuerlig beskärning*. Man kan trycka på tangenten :kbd:`Esc` för "
"att avbryta och göra en ny beskärning."
